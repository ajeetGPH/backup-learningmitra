<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';
if(isset($_POST['submit']) && $_POST['id']!="")
{	
$id=decrypt_string($_GET['id']);
$university_name = isset($_POST['university_name'])? $_POST['university_name']: false;	
$website_link = isset($_POST['website_link'])? $_POST['website_link']: false;	
$country_id = isset($_POST['country'])? $_POST['country']: false;	
$state_id = isset($_POST['state'])? $_POST['state']: false;	
$city_id = isset($_POST['city'])? $_POST['city']: false;	
$address = isset($_POST['address'])? $_POST['address']: false;	
$logo = isset($_POST['logo'])? $_POST['logo']: false;	
$map = isset($_POST['map'])? $_POST['map']: false;	
$history = isset($_POST['history'])? $_POST['history']: false;	
$global_rank = isset($_POST['global_rank'])? $_POST['global_rank']: false;	
$department_rank = isset($_POST['department_rank'])? $_POST['department_rank']: false;	
$graduation_type =isset($_POST['graduation_type'])? $_POST['graduation_type']: false;		
$department_id = isset($_POST['department'])? $_POST['department']: false;	
$admission_link = isset($_POST['admission_link'])? $_POST['admission_link']: false;	
$subject_id = isset($_POST['subject'])? $_POST['subject']: false;	
$course = isset($_POST['course'])? $_POST['course']: false;	
$course_link = isset($_POST['course_link'])? $_POST['course_link']: false;	
$applying_ref = isset($_POST['applying_ref'])? $_POST['applying_ref']: false;	
$tuition_instate = isset($_POST['tuition_instate'])? $_POST['tuition_instate']: false;	
$tuition_outstate = isset($_POST['tuition_outstate'])? $_POST['tuition_outstate']: false;	
$dept_link = isset($_POST['dept_link'])? $_POST['dept_link']: false;
$gre_score = isset($_POST['gre_score'])? $_POST['gre_score']: false;	
$gre_code = isset($_POST['gre_code'])? $_POST['gre_code']: false;	
$toefl_score = isset($_POST['toefl_score'])? $_POST['toefl_score']: false;	
$toefl_code = isset($_POST['toefl_code'])? $_POST['toefl_code']: false;	
$ielts_score = isset($_POST['ielts_score'])? $_POST['ielts_score']: false;	
$gmat_score = isset($_POST['gmat_score'])? $_POST['gmat_score']: false;	
$gmat_code = isset($_POST['gmat_code'])? $_POST['gmat_code']: false;	
$deadline_early_out = isset($_POST['deadline_early_out'])? $_POST['deadline_early_out']: false;	
$deadline_financial_out = isset($_POST['deadline_financial_out'])? $_POST['deadline_financial_out']: false;	
$deadline_regular_out = isset($_POST['deadline_regular_out'])? $_POST['deadline_regular_out']: false;	
$deadline_early_in = isset($_POST['deadline_early_in'])? $_POST['deadline_early_in']: false;	
$deadline_financial_in = isset($_POST['deadline_financial_in'])? $_POST['deadline_financial_in']: false;	
$deadline_regular_in = isset($_POST['deadline_regular_in'])? $_POST['deadline_regular_in']: false;	
$application_fee_in = isset($_POST['application_fee_in'])? $_POST['application_fee_in']: false;	
$application_fee_out = isset($_POST['application_fee_out'])? $_POST['application_fee_out']: false;	
$acceptence_rate = isset($_POST['acceptence_rate'])? $_POST['acceptence_rate']: false;	
$sop = isset($_POST['sop'])? $_POST['sop']: false;	
$resume = isset($_POST['resume'])? $_POST['resume']: false;	
$financial = isset($_POST['financial'])? $_POST['financial']: false;	
$financial_link = isset($_POST['financial_link'])? $_POST['financial_link']: false;	
$lor = isset($_POST['lor'])? $_POST['lor']: false;	
$transcript = isset($_POST['transcript'])? $_POST['transcript']: false;	

$overage_9to12 =  $_POST['overage_9to12'];
$undergraduate_cgpa =  $_POST['undergraduate_cgpa'];


$query_update_mysql_university=mysqli_query($con,"update universities set university_name='{$university_name}', website_link='{$website_link}', country_id='{$country_id}', state_id='{$state_id}', city_id='{$city_id}', address='{$address}', logo='{$logo}', map='{$map}', history='{$history}', global_rank='{$global_rank}', department_rank='{$department_rank}',graduation_type= '{$graduation_type}', department_id='{$department_id}', admission_link='{$admission_link}', subject_id='{$subject_id}', course='{$course}', course_link='{$course_link}', applying_ref='{$applying_ref}', tuition_instate='{$tuition_instate}', tuition_outstate='{$tuition_outstate}', dept_link='{$dept_link}', overage_9to12='{$overage_9to12}', undergraduate_cgpa='{$undergraduate_cgpa}', gre_score= '{$gre_score}', gre_code='{$gre_code}', toefl_score='{$toefl_score}', toefl_code= '{$toefl_code}', ielts_score='{$ielts_score}',gmat_score= '{$gmat_score}', gmat_code='{$gmat_code}', deadline_early_out='{$deadline_early_out}', deadline_financial_out='{$deadline_financial_out}', deadline_regular_out='{$deadline_regular_out}', deadline_early_in='{$deadline_early_in}', deadline_financial_in='{$deadline_financial_in}', deadline_regular_in='{$deadline_regular_in}', application_fee_in='{$application_fee_in}', application_fee_out='{$application_fee_out}', acceptence_rate='{$acceptence_rate}', sop='{$sop}', resume= '{$resume}', financial='{$financial}', financial_link='{$financial_link}', lor='{$lor}', transcript='{$transcript}' where id='".$id."'");
		if($query_update_mysql_university)
		{
			
			$message_success .="University Updated Successfully";

		}
		else
		{
			$error .=  "";
		}
		
}
	
if($_GET['id']!="")
{
	$id=decrypt_string($_GET['id']);
	$query=mysqli_query($con,"select * from universities where id='".$id."'");
	$result=mysqli_fetch_array($query);
}	
	


?>
		<script type="text/javascript">

$(document).ready(function() {
	$('#Loading').hide();    
});

function check_username(){

	var username = $("#username_check").val();
	if(username.length > 2){
		$('#Loading').show();
		$.post("check_student_name_availablity.php", {
			username: $('#username_check').val(),
		}, function(response){
			$('#Info').fadeOut();
			 $('#Loading').hide();
			setTimeout("finishAjax('Info', '"+escape(response)+"')", 450);
		});
		return false;
	}
}

function finishAjax(id, response){
 
  $('#'+id).html(unescape(response));
  $('#'+id).fadeIn(1000);
} 

</script>
<script>
 function getXMLHTTP() { 
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function getState(country_id) {		
		
		var strURL="state_find.php?country_id="+country_id;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('statediv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
	
	function getStates(val) {
	$.ajax({
		type: "POST",
		url: "getState.php",
		data:'country_id='+val,
		success: function(data){
			$("#state-list").html(data);
			getCity();
		}
	});
	}


	function getCity(val) {
	$.ajax({
		type: "POST",
		url: "getCity.php",
		data:'state_id='+val,
		success: function(data){
			$("#city-list").html(data);
		}
	});
	}
	
	function getsubject(val) {
$.ajax({
	type: "POST",
	url: "getsubject.php",
	data:'depardment_id='+val,
	success: function(data){
		$("#subject-list").html(data);
	}
});
}

$(document).ready(function(){
    if ($("select#graduation_type").val() == "Undergraduate") {
		$("div#Graduate").hide();
		$("div#Undergraduate").show();
	}else{
		if($("select#graduation_type").val() == "Graduate"){
			$("div#Undergraduate").hide();
			$("div#Graduate").show();
		}
	}
});
function select_form(val){
	if (val == "Undergraduate") {
		$("div#Graduate").hide();
		$("div#Undergraduate").show();
	}else{
		if(val == "Graduate"){
			$("div#Undergraduate").hide();
			$("div#Graduate").show();
		}
	}
}

</script>  
<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title"><i class="icon-male"></i>Manage University</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid padded">
		<div class="box">
			<div class="box-header">    	
				<ul class="nav nav-tabs nav-tabs-left">
					<li class="active">
						<a href="#add" data-toggle="tab"><i class="icon-pencil"></i>Edit University</a>
					</li>
				</ul>
				<div style="float:right;margin-top:3px;margin-right:3px"><a href="manage_university.php" class="btn btn-black"/><?php echo constant('TI_BUTTON_BACK');?> </a></div>
			</div>
	<div class="box-content padded">
		<div class="tab-content">        
			<?php include("message.php");?>
			<div class="tab-pane active" id="add" style="padding: 5px">
				<form method="post" action="" class="form-horizontal validatable" enctype="multipart/form-data">
                       
						<div class="padded">                   
							<div class="control-group">
								<label class="control-label">University Name</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="university_name" placeholder="University Name" value="<?php echo $result['university_name']?>" />&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Website link</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="website_link" placeholder="Website link" value="<?php echo $result['website_link']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Country</label>
								<div class="controls">      
									<select name="country" id="country-list" class="" onChange="getStates(this.value);">
										<option value disabled selected>Select Country</option>
										<?php
										$query_country =mysqli_query($con,"SELECT * FROM countries");

										while($country = mysqli_fetch_array($query_country))
										{
											?>
											<option value="<?php echo $country["id"]; ?>" <?php if($country['id']==$result['country_id']){echo "selected='selected'";}?>><?php echo $country["name"]; ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">State</label>
								<div class="controls">                                    
									<select name="state" id="state-list" class="demoInputBox" onChange="getCity(this.value);">
									<?php
									$query_state=mysqli_query($con,"select * from states where country_id='".$result['country_id']."'");?>
									
										<option value="">Select State</option>
										<?php
										while($res_state=mysqli_fetch_array($query_state))
										{
											?>
											<option value="<?php echo $res_state['id'];?>" <?php if($res_state['id']==$result['state_id']){echo "selected='selected'";}?>><?php echo $res_state['name']; ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">City</label>
								<div class="controls"> 
								<?php
									$query_city=mysqli_query($con,"select * from cities where id='".$result['city_id']."'");?>                                 
									<select name="city" id="city-list">
										<option value="">Select City</option>
										<?php
										while($res_city=mysqli_fetch_array($query_city))
										{
											?>
											<option value="<?php echo $res_city['id'];?>" <?php if($res_city['id']==$result['city_id']){echo "selected='selected'";}?>><?php echo $res_city['name']; ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Address</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="address" placeholder="Address" value="<?php echo $result['address']?>" />&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Logo</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="logo" placeholder="logo" value="<?php echo $result['logo']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Map</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="map" placeholder="" value="<?php echo $result['map']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">History</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="history" placeholder="History" value="<?php echo $result['history']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Global Rank</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="global_rank" placeholder="Global Rank" value="<?php echo $result['global_rank']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Department Rank</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="department_rank" placeholder="Department Rank" value="<?php echo $result['department_rank']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>


							<div class="control-group">
								<label class="control-label">Graduation Type</label>
								<div class="controls">                                    

									<select name="graduation_type" id="graduation_type" class="" onChange="select_form(this.value);">
										
										<option value="Undergraduate" <?php if ($result['graduation_type'] == "Undergraduate"){ echo 'selected="selected"';}?>>
											Undergraduate
										</option>
										
											?><option value="Graduate" <?php if ($result['graduation_type'] == "Graduate"){ echo 'selected="selected"';}?>>
											Graduate
										</option>
										
									
									</select>
								</div>
							</div>

							<div id="select_form">
								
							</div>

							<div class="control-group">
								<label class="control-label">Department</label>
								<div class="controls">                                    

									<select name="department" id="department-list" class="" onChange="getsubject(this.value);">
										<option value disabled selected>Select Department</option>
										<?php
										$query_Department =mysqli_query($con,"SELECT * FROM uni_department");

										while($Department = mysqli_fetch_array($query_Department))
										{
											?>
											<option value="<?php echo $Department["id"]; ?>" <?php if($Department['id']==$result['department_id']){echo "selected='selected'";}?>><?php echo $Department["name"]; ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Subject</label>
								<div class="controls">                                    

									<select name="subject" id="subject-list" class="" onChange="">
										<?php
									$query_subject=mysqli_query($con,"select * from uni_subjects where depardment_id='".$result['department_id']."'");?>
									
										<option value="">Select Subject</option>
										<?php
										while($res_subject=mysqli_fetch_array($query_subject))
										{
											?>
											<option value="<?php echo $res_subject['id'];?>" <?php if($res_subject['id']==$result['subject_id']){echo "selected='selected'";}?>><?php echo $res_subject['name']; ?></option>
											<?php
										}
										?>
									</select>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Course</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="course" placeholder="Course" value="<?php echo $result['course']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Admission Link</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="admission_link" placeholder="Admission Link" value="<?php echo $result['admission_link']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tuition(instate)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="tuition_instate" placeholder="Tuition(instate)" value="<?php echo $result['tuition_instate']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tuition(Outstate)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="tuition_outstate" placeholder="Tuition(Outstate)" value="<?php echo $result['tuition_outstate']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Dept Link</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="dept_link" placeholder="Dept Link" value="<?php echo $result['dept_link']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group" id="Undergraduate">
								<label class="control-label" >Average of 9,10,11,12</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gre_score" placeholder="Average" value="<?php echo $result['overage_9to12']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group" id="Graduate">
								<label class="control-label">Undergraduate CGPA</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gre_score" placeholder="Undergraduate CGPA" value="<?php echo $result['undergraduate_cgpa']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">GRE - Score</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gre_score" placeholder="GRE - Score" value="<?php echo $result['gre_score']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>

							<div class="control-group">
								<label class="control-label">GRE - Code</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gre_code" placeholder="GRE - Code" value="<?php echo $result['gre_code']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">TOEFL- Score</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="toefl_score" placeholder="TOEFL- Score" value="<?php echo $result['toefl_score']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Toefl- Code</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="toefl_code" placeholder="Toefl- Code" value="<?php echo $result['toefl_code']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">IELTS- Score</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="ielts_score" placeholder="IELTS- Score" value="<?php echo $result['ielts_score']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">GMAT- Score</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gmat_score" placeholder="GMAT- Score" value="<?php echo $result['gmat_score']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">GMAT - Code</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="gmat_code" placeholder="GMAT - Code" value="<?php echo $result['gmat_code']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - early(OUT)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_early_out" placeholder="Deadline - early(OUT)" value="<?php echo $result['deadline_early_out']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - financial(OUT)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_financial_out" placeholder="Deadline - financial(OUT)" value="<?php echo $result['deadline_financial_out']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - regular (OUT)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_regular_out" placeholder="Deadline - regular (OUT)" value="<?php echo $result['deadline_regular_out']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - early(IN)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_early_in" placeholder="Deadline - early(IN)" value="<?php echo $result['deadline_early_in']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - financial(IN)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_financial_in" placeholder="Deadline - financial(IN)" value="<?php echo $result['deadline_financial_in']?>"/>&nbsp;&nbsp;<br/><br/>



								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Deadline - regular (IN)</label>
								<div class="controls">                                    


									<input type="text" class="validate[required]" name="deadline_regular_in" placeholder="Deadline - regular (IN)" value="<?php echo $result['deadline_regular_in']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Application fee(IN)</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="application_fee_in" placeholder="Application fee(IN)" value="<?php echo $result['application_fee_in']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Application fee(OUT)</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="application_fee_out" placeholder="Application fee(OUT)" value="<?php echo $result['application_fee_out']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Acceptence rate</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="acceptence_rate" placeholder="Acceptence rate" value="<?php echo $result['acceptence_rate']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Course Link</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="course_link" placeholder="Course Link" value="<?php echo $result['course_link']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Applying /Cross ref (link)</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="applying_ref" placeholder="Applying /Cross ref (link)" value="<?php echo $result['applying_ref']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">SOP</label>
								<div class="controls">                                    
									<select name="sop" id="" class="" onChange="">
										<option value="Yes">
											Yes
										</option>
										<option value="No">
											No
										</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Resume</label>
								<div class="controls">                                    
									<select name="resume" id="" class="" onChange="">
										<option value="Yes">
											Yes
										</option>
										<option value="No">
											No
										</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Financial</label>
								<div class="controls">                                    
									<select  name="financial" id="" class="" onChange="">
										<option value="Yes">
											Yes
										</option>
										<option value="No">
											No
										</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Financial (LINK)</label>
								<div class="controls">                                    
									<input type="text" class="validate[required]" name="financial_link" placeholder="Acceptence rate" value="<?php echo $result['financial_link']?>"/>&nbsp;&nbsp;<br/><br/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">LOR</label>
								<div class="controls">                                    
									<select name="lor" id="" class="" onChange="">
										<option value="1">
											1
										</option>
										<option value="2">
											2
										</option>
										<option value="3">
											3
										</option>
										<option value="4">
											4
										</option>
									</select>

								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Transcript</label>
								<div class="controls">                                    

									<select name="transcript" id="" class="" onChange="">
										<option value="Yes">
											Yes
										</option>
										<option value="No">
											No
										</option>
									</select>
								</div>
							</div>
						</div>



                        <div class="form-actions">
                            <button type="submit" class="btn btn-gray">Edit University</button>
							<input type="hidden" value="Update_setting" name="submit">
							<input type="hidden" value="<?php echo $_GET['id'];?>" name="id">
                        </div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS--->
            
		</div>
	</div>
</div>
</div>       
           <?php include("copyright.php");?>
		   </div>
	</div>
</body> 
</html>