<?php
include('include/configure.php');
include('login_check.php');

if (isset($_POST['favourite_list']) && isset($_POST['universities_id'])) 
{ 
	$universities_id = isset($_POST['universities_id'])? $_POST['universities_id']: false;
	$user_id = $_SESSION['admin_id'];
	$query_insert = "INSERT INTO targeted_universities (user_id, universities_id) VALUES('{$user_id}', '".$universities_id."') ON DUPLICATE KEY UPDATE universities_id = '".$universities_id."' ";

	if (mysqli_query($con, $query_insert)) {

		echo "University Added Successfully To Favourite List";
	}else {
			//echo "Error: " . $query . "<br>" . mysqli_error($con);
	}
} else{

if (isset($_POST['search'])) 
{ 

	$user_id = $_SESSION['admin_id'];
	$search_count = " SELECT * FROM targeted_universities where user_id = '".$user_id."' and search_count = '1'";
	$search_count_query = mysqli_query($con,$search_count); 
	if (mysqli_num_rows($search_count_query) > 0){ ?>
		<div>
			<h4 style="color: #9e3535">You already searched.</h4>
		</div>
	<?php }else{

	$gre_score = isset($_POST['gre_score'])? $_POST['gre_score']: false;	
	$toefl_score =isset($_POST['toefl_score'])? $_POST['toefl_score']: false;		
	$ielts_score = isset($_POST['ielts_score'])? $_POST['ielts_score']: false;	
	$gmat_score = isset($_POST['gmat_score'])? $_POST['gmat_score']: false;	
	$undergraduate_cgpa = isset($_POST['undergraduate_cgpa'])? $_POST['undergraduate_cgpa']: false;	
	$status=1;

$query = "SELECT * FROM universities where gre_score <= '".$gre_score."' and toefl_score <= '".$toefl_score."' and ielts_score <= '".$ielts_score."' and gmat_score <= '".$gmat_score."' and undergraduate_cgpa <= '".$undergraduate_cgpa."' and status = '".$status."'";
$query_result = mysqli_query($con,$query); 

?>
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
						Suggested Universities</h3>
				</div>
			</div>
		</div>
	</div>
		<?php if (mysqli_num_rows($query_result) > 0) { 
			$search_count_update = "update targeted_universities set search_count = '1' where user_id = '$user_id'";
 			mysqli_query($con,$search_count_update);
			?>       
		<div class="container-fluid padded">
			<div class="box">
				
				<div class="box-content padded">
					<div class="tab-content">        

						<div class="tab-pane box active" id="list">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>
										<th><div>University Name</div></th>   
										<th><div>Rank</div></th> 
										<th><div>Country</div></th> 
										<th><div>Department</div></th> 
										<th><div>Admission</div></th> 
										<th><div>Add to favourite</div></th> 
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=0;
									while($universities = mysqli_fetch_array($query_result))
									{ $i++;
											$query =mysqli_query($con,"SELECT * FROM countries where id = '".$universities['country_id']."'");
											$country = mysqli_fetch_array($query)
										?>
										<tr>
											<td><?php echo $universities['university_name'];?> </td>
											<td><?php echo $universities['global_rank'];?> </td>
											<td><?php echo $country['name'];?> </td>
											<td><?php echo $universities['dept_link'];?> </td>
											<td><?php echo $universities['admission_link'];?> </td>
											<td align="center">
												<input type="checkbox" class='roles' name="favourite[]" id="favourite" value="<?php echo $universities['id'];?>">
												
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
							<div class="form-actions">
								<input type="hidden" id="universities_id" name="universities_id" value="">
								<input type="button" name="Generate_Report" id="Generate_Report" value="Generate Report" >
								<p id="fav_status"></p>
							</div>
							
						</div>
					</div>
			</div>
		</div>
		<div id="university_report">
			<a href="uni_doc_required.php" class="btn btn-blue">Required Documents</a>
			<a href="application_links.php" class="btn btn-blue">Application Links</a>
			<a href="university_info.php" class="btn btn-blue">University Info.</a>
		</div>
	</div>
	<script type="text/javascript">
		
	$("#university_report").hide();
		
	$('#Generate_Report').click(validate);

	function validate(){
   		if($('.roles:checkbox:checked').length == 0)
		{
			alert("Please Select at least one University from list");	
			 return false;
		}else { 
				
				$("#university_report").show();

			 }       
	}

$('.roles:checkbox').change(addToFav);

	function addToFav(){
				var checkedVals = $('.roles:checkbox:checked').map(function() {
				    return this.value;
				}).get();
				
				$('#universities_id').attr('value', checkedVals);
				universities_id = $('#universities_id').val();
				$.ajax({
					type: "POST",
					url: "suggested_university.php",
					data:{universities_id: universities_id, favourite_list: 'favourite_list'},
					success: function(data){
						$("#fav_status").html(data);
					}
				});

		 }       

</script>
<?php  } else{ ?>
<div>
	<h4 style="color: #9e3535">Not found</h4>
</div>
<?php } } } }?>