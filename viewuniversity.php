<?Php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');

if($_GET['id']!="")
{
	$id=$_GET['id'];	
	$query=mysqli_query($con,"select * from universities where id='{$id}'");
	$res=mysqli_fetch_array($query);
	
}

?>

<body>
  
<div class="tab-pane box active" id="edit" style="padding: 5px">
	<div class="box-content">
		<form action="<?php $PHP_SELF ?>" method="post" accept-charset="utf-8" class="form-horizontal validatable" target="_top" enctype="multipart/form-data">
						<div class="padded" >                   
                            <div class="control-group">
                                <label class="control-label">University Name</label>
                                <div class="controls" style="padding-top:5px">
								<?php                                   
								   echo $res['university_name'];
								   ?>
                                </div>
                            </div>
                        </div>			
						

						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Website link</label>
                                <div class="controls"  style="padding-top:5px" >                                  
								   <?php 
								   
								    echo $res['website_link'];							 
								   ?>
                                </div>
                            </div>
                        </div>

						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Country</label>
                                <div class="controls"  style="padding-top:5px">                                  
								  <?php 
                                    $country_id = $res['country_id'];
                                    $query=mysqli_query($con,"select * from countries where id='{$country_id}'");
                                    $country=mysqli_fetch_array($query);
                                    echo $country['name'];?>
                                </div>
                            </div>
                        </div>

                         
                         <div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">State</label>
                                <div class="controls"  style="padding-top:5px">
									<?php $state_id = $res['state_id'];
                                    $query=mysqli_query($con,"select * from states where id='{$state_id}'");
                                    $state=mysqli_fetch_array($query);
                                    echo $state['name'];?>
                                </div>
                            </div>
                        </div>
                        
                             
						 <div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">City</label>
                                <div class="controls"  style="padding-top:5px">
									<?php $city_id = $res['city_id'];
                                    $query=mysqli_query($con,"select * from cities where id='{$city_id}'");
                                    $city=mysqli_fetch_array($query);
                                    echo $city['name'];?>
                                </div>
                            </div>
                        </div>

						
						 
									
						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Address</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['address'];?>
                                </div>
                            </div>
                        </div>

						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Logo</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['logo'];?>
                                </div>
                            </div>
                        </div>
                        <div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Map</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['map'];?>
                                </div>
                            </div>
                        </div>
						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">History</label>
                                <div class="controls"  style="padding-top:5px">
                                   <?php 
									echo $res['history'];?>				 
                                </div>
                            </div>
                        </div>
						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Global Rank</label>
                                <div class="controls"  style="padding-top:5px">
								<?php
								echo $res['global_rank'];
								?>
                                </div> 
                            </div>
                        </div>

						<div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Department Rank</label>
                                <div class="controls"  style="padding-top:5px">
									<?php echo $res['department_rank'];?>
                                </div>
                            </div>
                        </div>

						

						

						
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Admission Link</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['admission_link'];?>
                                </div>
                            </div>
                         </div>
                         <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Graduation Type</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['graduation_type'];?>
                                </div>
                            </div>
                         </div>
                         

                         <div class="padded">                   
                            <div class="control-group">
                                <label class="control-label">Department</label>
                                <div class="controls"  style="padding-top:5px">
                                     <?php 
                                    $department_id = $res['department_id'];
                                    $query=mysqli_query($con,"select * from uni_department where id='{$department_id}'");
                                    $Department=mysqli_fetch_array($query);
                                    echo $Department['name'];?>
                                </div>
                            </div>
                        </div>

						 <div class="padded">  
						<div class="control-group">
							<label class="control-label">Subject</label>
							<div class="controls"  style="padding-top:5px">
							<?php 
                                    $subject_id = $res['subject_id'];
                                    $query=mysqli_query($con,"select * from uni_subjects where id='{$subject_id}'");
                                    $Subject=mysqli_fetch_array($query);
                                    echo $Subject['name'];?>
							</div>
						</div>
						</div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Course</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['course'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Course Link</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['course_link'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Applying Ref</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['applying_ref'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Tuition Instate</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['tuition_instate'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Tuition Outstate</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['tuition_outstate'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Dept Link</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['dept_link'];?>
                                </div>
                            </div>
                         </div>
                         <?php
                         if ($res['graduation_type'] == "Undergraduate") {
                             ?>
                             <div class="padded">  
                                <div class="control-group">
                                    <label class="control-label">Average of 9,10,11,12</label>
                                    <div class="controls"  style="padding-top:5px">
                                        <?php echo $res['overage_9to12'];?>
                                    </div>
                                </div>
                             </div>
                             <?php }
                             else{
                                if ($res['graduation_type'] == "Graduate") { ?>
                                     <div class="padded">  
                                        <div class="control-group">
                                            <label class="control-label">Undergraduate CGPA</label>
                                            <div class="controls"  style="padding-top:5px">
                                                <?php echo $res['undergraduate_cgpa'];?>
                                            </div>
                                        </div>
                                     </div>
                             <?php
                                    }
                                 }
                             
                             ?>
                             
                        
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">GRE Score</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['gre_score'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">GRE Code</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['gre_code'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Toefl Score</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['toefl_score'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Toefl Code</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['toefl_code'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">ielts Score</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['ielts_score'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Gmat Score</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['gmat_score'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Gmat Code</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['gmat_code'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - early(OUT)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_early_out'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - financial(OUT)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_financial_out'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - regular (OUT)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_regular_out'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - early(IN)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_early_in'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - financial(IN)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_financial_in'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Deadline - regular (IN)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['deadline_regular_in'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Application fee(IN)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['application_fee_in'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Application fee(OUT)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['application_fee_out'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Acceptence rate</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['acceptence_rate'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">SOP</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['sop'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Resume</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['resume'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Financial</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['financial'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Financial (LINK)</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['financial_link'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">LOR</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['lor'];?>
                                </div>
                            </div>
                         </div>
                        <div class="padded">  
                            <div class="control-group">
                                <label class="control-label">Transcript</label>
                                <div class="controls"  style="padding-top:5px">
                                    <?php echo $res['transcript'];?>
                                </div>
                            </div>
                         </div>

		</form>
	</div>
</div>

</body></html>