<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';

if (isset($_POST['submit'])) 
{ 


	$university_name = isset($_POST['university_name'])? $_POST['university_name']: false;	
	$website_link = isset($_POST['website_link'])? $_POST['website_link']: false;	
	$country_id = isset($_POST['country'])? $_POST['country']: false;	
	$state_id = isset($_POST['state'])? $_POST['state']: false;	
	$city_id = isset($_POST['city'])? $_POST['city']: false;	
	$address = isset($_POST['address'])? $_POST['address']: false;	
	$logo = isset($_POST['logo'])? $_POST['logo']: false;	
	$map = isset($_POST['map'])? $_POST['map']: false;	
	$history = isset($_POST['history'])? $_POST['history']: false;	
	$global_rank = isset($_POST['global_rank'])? $_POST['global_rank']: false;	
	$department_rank = isset($_POST['department_rank'])? $_POST['department_rank']: false;
	$graduation_type =isset($_POST['graduation_type'])? $_POST['graduation_type']: false;		
	$department_id = isset($_POST['department'])? $_POST['department']: false;	
	$admission_link = isset($_POST['admission_link'])? $_POST['admission_link']: false;	
	$subject_id = isset($_POST['subject'])? $_POST['subject']: false;	
	$course = isset($_POST['course'])? $_POST['course']: false;	
	$course_link = isset($_POST['course_link'])? $_POST['course_link']: false;	
	$applying_ref = isset($_POST['applying_ref'])? $_POST['applying_ref']: false;	
	$tuition_instate = isset($_POST['tuition_instate'])? $_POST['tuition_instate']: false;	
	$tuition_outstate = isset($_POST['tuition_outstate'])? $_POST['tuition_outstate']: false;	
	$dept_link = isset($_POST['dept_link'])? $_POST['dept_link']: false;	
	$gre_score = isset($_POST['gre_score'])? $_POST['gre_score']: false;	
	$gre_code = isset($_POST['gre_code'])? $_POST['gre_code']: false;	
	$toefl_score = isset($_POST['toefl_score'])? $_POST['toefl_score']: false;	
	$toefl_code = isset($_POST['toefl_code'])? $_POST['toefl_code']: false;	
	$ielts_score = isset($_POST['ielts_score'])? $_POST['ielts_score']: false;	
	$gmat_score = isset($_POST['gmat_score'])? $_POST['gmat_score']: false;	
	$gmat_code = isset($_POST['gmat_code'])? $_POST['gmat_code']: false;	
	$deadline_early_out = isset($_POST['deadline_early_out'])? $_POST['deadline_early_out']: false;	
	$deadline_financial_out = isset($_POST['deadline_financial_out'])? $_POST['deadline_financial_out']: false;	
	$deadline_regular_out = isset($_POST['deadline_regular_out'])? $_POST['deadline_regular_out']: false;	
	$deadline_early_in = isset($_POST['deadline_early_in'])? $_POST['deadline_early_in']: false;	
	$deadline_financial_in = isset($_POST['deadline_financial_in'])? $_POST['deadline_financial_in']: false;	
	$deadline_regular_in = isset($_POST['deadline_regular_in'])? $_POST['deadline_regular_in']: false;	
	$application_fee_in = isset($_POST['application_fee_in'])? $_POST['application_fee_in']: false;	
	$application_fee_out = isset($_POST['application_fee_out'])? $_POST['application_fee_out']: false;	
	$acceptence_rate = isset($_POST['acceptence_rate'])? $_POST['acceptence_rate']: false;	
	$sop = isset($_POST['sop'])? $_POST['sop']: false;	
	$resume = isset($_POST['resume'])? $_POST['resume']: false;	
	$financial = isset($_POST['financial'])? $_POST['financial']: false;	
	$financial_link = isset($_POST['financial_link'])? $_POST['financial_link']: false;	
	$lor = isset($_POST['lor'])? $_POST['lor']: false;	
	$transcript = isset($_POST['transcript'])? $_POST['transcript']: false;	
	$status=1;
	$overage_9to12 =  $_POST['overage_9to12'];
	$undergraduate_cgpa =  $_POST['undergraduate_cgpa'];
	


	$query = "INSERT INTO universities (university_name, website_link, country_id, state_id, city_id, address, logo, map, history, global_rank, department_rank,graduation_type, department_id, admission_link, subject_id, course, course_link, applying_ref, tuition_instate, tuition_outstate, dept_link, overage_9to12, undergraduate_cgpa, gre_score, gre_code, toefl_score, toefl_code, ielts_score,gmat_score, gmat_code, deadline_early_out, deadline_financial_out, deadline_regular_out, deadline_early_in, deadline_financial_in, deadline_regular_in, application_fee_in, application_fee_out, acceptence_rate, sop, resume, financial, financial_link, lor, transcript, status) VALUES('{$university_name}', '{$website_link}', '{$country_id}', '{$state_id}', '{$city_id}', '{$address}', '{$logo}', '{$map}', '{$history}', '{$global_rank}', '{$department_rank}', '{$graduation_type}', '{$department_id}', '{$admission_link}', '{$subject_id}', '{$course}', '{$course_link}', '{$applying_ref}', '{$tuition_instate}', '{$tuition_outstate}', '{$dept_link}', '{$overage_9to12}', '{$undergraduate_cgpa}', '{$gre_score}', '{$gre_code}', '{$toefl_score}', '{$toefl_code}', '{$ielts_score}', '{$gmat_score}', '{$gmat_code}', '{$deadline_early_out}', '{$deadline_financial_out}', '{$deadline_regular_out}', '{$deadline_early_in}', '{$deadline_financial_in}', '{$deadline_regular_in}', '{$application_fee_in}', '{$application_fee_out}', '{$acceptence_rate}', '{$sop}', '{$resume}', '{$financial}', '{$financial_link}', '{$lor}', '{$transcript}', '{$status}')";

	if (mysqli_query($con, $query)) {

		$message_success .= "University Added Successfully";
	}else {
		echo "Error: " . $query . "<br>" . mysqli_error($con);
	}
}

?>
<script>
	function getStates(val) {
		$.ajax({
			type: "POST",
			url: "getState.php",
			data:'country_id='+val,
			success: function(data){
				$("#state-list").html(data);
				getCity();
			}
		});
	}


	function getCity(val) {
		$.ajax({
			type: "POST",
			url: "getCity.php",
			data:'state_id='+val,
			success: function(data){
				$("#city-list").html(data);
			}
		});
	}
	function getsubject(val) {
		$.ajax({
			type: "POST",
			url: "getsubject.php",
			data:'depardment_id='+val,
			success: function(data){
				$("#subject-list").html(data);
			}
		});
	}
	$(document).ready(function(){
		$("div#Graduate").hide();
		$("div#Undergraduate").show();
	});
	function select_form(val){
		if (val == "Undergraduate") {
			$("div#Graduate").hide();
			$("div#Undergraduate").show();
		}else{
			if(val == "Graduate"){
				$("div#Undergraduate").hide();
				$("div#Graduate").show();
			}
		}
	}



</script>
<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
						<?php echo constant('TI_MAIN_HEADING_UNIVERSITY');?> </h3>
					</div>
				</div>
			</div>
		</div>       
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				<div class="box-header">    
					<ul class="nav nav-tabs nav-tabs-left">
						<li class="active">
							<a href="#list" data-toggle="tab"><i class="icon-align-justify"></i><?php echo constant('TI_SUB_HEADING1_UNIVERSITY');?></a>
						</li>
						<li>
							<a href="#add" data-toggle="tab"><i class="icon-plus"></i><?php echo constant('TI_SUB_HEADING2_UNIVERSITY');?></a>
						</li>
						<li>
							<a href="search_university.php">Search University</a>
						</li>
					</ul> 
				</div>
				<div class="box-content padded">
					<div class="tab-content">        
						<div class="tab-pane box active" id="list">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>
										<th><div>University Name</div></th>   
										<th><div>Website link</div></th> 
										<th><div>Graduation Type</div></th> 
										<th><div>Address</div></th> 
										<th><div><?php echo constant('TI_TABLE_HEADING_STATUS');?></div></th> 
										<th><div><?php echo constant('TI_TABLE_HEADING_OPTIONS');?></div></th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$query=mysqli_query($con,"select * from universities");
									$i=0;
									while($row=mysqli_fetch_array($query))
									{ 

										$country=mysqli_fetch_array(mysqli_query($con,"select * from catigories_description where categories_id='".$row['category_id']."' and languages_id='".$_SESSION['admin_languages_id']."'"));

										$query_subcategory_name=mysqli_fetch_array(mysqli_query($con,"select * from subcategories_description where subcategories_id='".$row['subcategories_id']."' and languages_id='".$_SESSION['admin_languages_id']."'"));



										$query_center_name=mysqli_fetch_array(mysqli_query($con,"select institution_name from center where center_id='".$row['center_id']."'"));
										$i++;

										?>
										<tr>
											<td><?php echo $row['university_name'];?> </td>
											<td><?php echo $row['website_link'];?> </td>
											<td><?php echo $row['graduation_type'];?> </td>
											<td><?php echo $row['address'];?> </td>
											<td>
												<?php
												if($row['status']==1)
													{?>													
														<a data-toggle="modal" href="#modal-status-deactive" onclick="modal_status_deactive('university_status.php?id=<?php echo encrypt_string($row['id']);?>')" class="btn btn-red btn-small"><i class="icon-eye-close"></i> <?php echo constant('TI_BUTTON_DEACTIVATE');?></a>
													<?php }
													else
														{?>														<a data-toggle="modal" href="#modal-status-active" onclick="modal_status_active('university_status.php?id=<?php echo encrypt_string($row['id']);?>')" class="btn btn-green btn-small"><i class="icon-eye-open"></i> <?php echo constant('TI_BUTTON_ACTIVATE');?></a>
												<?php }?>
											</td>
											<td align="center">
												<a data-toggle="modal" href="university_edit.php?id=<?php echo encrypt_string($row['id']);?>" class="btn btn-gray btn-small"><i class="icon-pencil"></i> <?php echo constant('TI_BUTTON_EDIT');?></a>

												<a data-toggle="modal" href="#modal-delete" onclick="modal_delete('universitydelete.php?id=<?php echo encrypt_string($row['id']);?>')" class="btn btn-red btn-small"><i class="icon-trash"></i> <?php echo constant('TI_BUTTON_DELETE');?> </a>

												<a data-toggle="modal" href="#university-modal-form" onclick="modal_view_universiity('View_Universiity',<?php echo $row['id'];?>)" class="btn btn-blue btn-small"><i class="icon-zoom-in"></i> <?php echo constant('TI_BUTTON_VIEW');?></a>

												<!-- <a data-toggle="modal" target="_new" href="result-list.php?student_id=<?php echo encrypt_string($row['student_id']);?>&category_id=<?php echo encrypt_string($row['category_id']);?>&center_id=<?php echo encrypt_string($row['center_id']);?>&pa=1" class="btn btn-gray btn-small"><i class="icon-list-ol"></i> <?php echo constant('TI_BUTTON_MAIN_RESUTLT_LIST_OF_ATTEMPEXAM'); ?></a> -->

											</td>
										</tr>
									<?php } ?>

								</tbody>
							</table>
						</div>

						<div class="tab-pane box" id="add" style="padding: 5px">
							<div class="box-content">
								<form action="<?php $PHP_SELF ?>" method="post" accept-charset="utf-8" class="form-horizontal validatable" target="_top">	
									<div class="padded">                   
										<div class="control-group">
											<label class="control-label">University Name</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="university_name" placeholder="University Name"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Website link</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="website_link" placeholder="Website link"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Country</label>
											<div class="controls">      
												<select name="country" id="country-list" class="" onChange="getStates(this.value);">
													<option value disabled selected>Select Country</option>
													<?php
													$query =mysqli_query($con,"SELECT * FROM countries");

													while($country = mysqli_fetch_array($query))
													{
														?>
														<option value="<?php echo $country["id"]; ?>"><?php echo $country["name"]; ?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">State</label>
											<div class="controls">                                    

												<select name="state" id="state-list" class="demoInputBox" onChange="getCity(this.value);">
													<option value="">Select State</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">City</label>
											<div class="controls">                                  
												<select name="city" id="city-list">
													<option value="">Select City</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Address</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="address" placeholder="Address"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Logo</label>
											<div class="controls">                                    


												<input type="text" class="" name="logo" placeholder="logo"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Map</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="map" placeholder=""/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">History</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="history" placeholder="History"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Global Rank</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="global_rank" placeholder="Global Rank"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Department Rank</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="department_rank" placeholder="Department Rank"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>


										<div class="control-group">
											<label class="control-label">Graduation Type</label>
											<div class="controls">                                    

												<select name="graduation_type" id="graduation_type" class="validate[required]" onChange="select_form(this.value);">
													<option value="">
														Select
													</option>
													<option value="Undergraduate">
														Undergraduate
													</option>
													<option value="Graduate">
														Graduate
													</option>
												</select>
											</div>
										</div>

										<div id="select_form">

										</div>


										<div class="control-group">
											<label class="control-label">Department</label>
											<div class="controls">                                    

												<select name="department" class="validate[required]" id="department-list" class="" onChange="getsubject(this.value);">
													<option value disabled selected>Select Department</option>
													<?php
													$query =mysqli_query($con,"SELECT * FROM uni_department");

													while($department = mysqli_fetch_array($query))
													{
														?>
														<option value="<?php echo $department["id"]; ?>"><?php echo $department["name"]; ?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Subject</label>
											<div class="controls">                                    


												<select name="subject" class="validate[required]" id="subject-list" class="" >
													<option value="" selected>Select Subject</option>

												</select>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Course</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="course" placeholder="Course"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Course Link</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="course_link" placeholder="Course Link"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Dept Link</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="dept_link" placeholder="Dept Link"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Admission Link</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="admission_link" placeholder="Admission Link"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Tuition(instate)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="tuition_instate" placeholder="Tuition(instate)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Tuition(Outstate)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="tuition_outstate" placeholder="Tuition(Outstate)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										
										<div class="control-group" id="Undergraduate">
											<label class="control-label">Average of 9,10,11,12</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="overage_9to12" placeholder="Average"/>&nbsp;&nbsp;<br/><br/>


											</div>
										</div>
										<div class="control-group" id="Graduate">
											<label class="control-label">Undergraduate CGPA</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="undergraduate_cgpa" placeholder="Undergraduate CGPA"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">GRE - Score</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="gre_score" placeholder="GRE - Score"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>

										<div class="control-group">
											<label class="control-label">GRE - Code</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="gre_code" placeholder="GRE - Code"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">TOEFL- Score</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="toefl_score" placeholder="TOEFL- Score"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Toefl- Code</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="toefl_code" placeholder="Toefl- Code"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">IELTS- Score</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="ielts_score" placeholder="IELTS- Score"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">GMAT- Score</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="gmat_score" placeholder="GMAT- Score"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">GMAT - Code</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="gmat_code" placeholder="GMAT - Code"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Deadline - early(OUT)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_early_out" placeholder="Deadline - early(OUT)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Deadline - early(IN)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_early_in" placeholder="Deadline - early(IN)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Deadline - financial(OUT)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_financial_out" placeholder="Deadline - financial(OUT)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Deadline - financial(IN)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_financial_in" placeholder="Deadline - financial(IN)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Deadline - regular (OUT)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_regular_out" placeholder="Deadline - regular (OUT)"/>&nbsp;&nbsp;<br/><br/>



											</div>
										</div>
										
										
										<div class="control-group">
											<label class="control-label">Deadline - regular (IN)</label>
											<div class="controls">                                    


												<input type="text" class="validate[required]" name="deadline_regular_in" placeholder="Deadline - regular (IN)"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Application fee(OUT)</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="application_fee_out" placeholder="Application fee(OUT)"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Application fee(IN)</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="application_fee_in" placeholder="Application fee(IN)"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label">Acceptence rate</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="acceptence_rate" placeholder="Acceptence rate"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label">Applying /Cross ref (link)</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="applying_ref" placeholder="Applying /Cross ref (link)"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">SOP</label>
											<div class="controls">                                    
												<select name="sop" id="" class="" onChange="">
													<option value="Yes">
														Yes
													</option>
													<option value="No">
														No
													</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Resume</label>
											<div class="controls">                                    
												<select name="resume" id="" class="" onChange="">
													<option value="Yes">
														Yes
													</option>
													<option value="No">
														No
													</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Financial</label>
											<div class="controls">                                    
												<select  name="financial" id="" class="" onChange="">
													<option value="Yes">
														Yes
													</option>
													<option value="No">
														No
													</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Financial (LINK)</label>
											<div class="controls">                                    
												<input type="text" class="validate[required]" name="financial_link" placeholder="financial link"/>&nbsp;&nbsp;<br/><br/>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">LOR</label>
											<div class="controls">                                    
												<select name="lor" id="" class="" onChange="">
													<option value="1">
														1
													</option>
													<option value="2">
														2
													</option>
													<option value="3">
														3
													</option>
													<option value="4">
														4
													</option>
												</select>

											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Transcript</label>
											<div class="controls">                                    

												<select name="transcript" id="" class="" onChange="">
													<option value="Yes">
														Yes
													</option>
													<option value="No">
														No
													</option>
												</select>
											</div>
										</div>


									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-gray"><?php echo constant('TI_BUTTON_SUBCATEGORY_ADD_UNIVERSITY');?></button>
										<input type="hidden" value="Add new university" name="submit">
									</div>
								</form>                
							</div>                
						</div>
						<!----CREATION FORM ENDS--->

					</div>
				</div>
			</div>            
		</div>       
		<?php include("copyright.php");?>
	</div>
</div>
</body>

<!-----------HIDDEN MODAL FORM - COMMON IN ALL PAGES ------>
<div id="university-modal-form" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<div id="modal-tablesLabel-university" style="color:#fff; font-size:16px;">&nbsp; </div>
	</div>
	<div class="modal-body" id="modal-body-university"><?php echo constant('TI_LOADING_DATA');?></div>
	<div class="modal-footer">
		<!-- <button class="btn btn-gray" onclick="custom_print('frame1')"><?php echo constant('TI_PRINT');?></button> -->
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CLOSE');?></button>
	</div>
</div>
<!-----------HIDDEN MODAL DELETE CONFIRMATION - COMMON IN ALL PAGES ------>
<div id="modal-delete" class="modal hide fade" style="height:140px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
	</div>
	<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_POPUP_SUBCATEGORY_DELETE');?></div>
	<div class="modal-footer">
		<a href="subcategory_delete.php?subcategories_id=<?php echo $row['subcategories_id'];?>" id="delete_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
	</div>
</div>


<!-----------HIDDEN MODAL ACTIVE STATUS CONFIRMATION - COMMON IN ALL PAGES ------>

<div id="modal-status-active" class="modal hide fade" style="height:140px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
	</div>
	<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_ACTIVE');?></div>
	<div class="modal-footer">
		<a href="$query_category.php?subcategories_id=<?php echo $row['subcategories_id'];?>" id="active_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
	</div>
</div>


<div id="modal-status-deactive" class="modal hide fade" style="height:140px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
	</div>
	<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_DEACTIVE');?></div>
	<div class="modal-footer">
		<a href="$query_category.php?subcategories_id=<?php echo $row['subcategories_id'];?>" id="deactive_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
	</div>
</div>






<script>

	function modal_view_universiity(param1,param2 )
	{
		document.getElementById('modal-body-university').innerHTML = 
		'<iframe id="frame1" src="viewuniversity.php?id='+param2+'" width="100%" height="400" frameborder="0"></iframe>';
		document.getElementById('modal-tablesLabel-university').innerHTML = param1.replace("_"," ");
	}

	function modal(param1 ,param2 ,param3)
	{
		document.getElementById('modal-body').innerHTML = 
		'<iframe id="frame1" src="category-edit.php?c_id='+param2+'" width="100%" height="100%" frameborder="0"></iframe>';
		document.getElementById('modal-tablesLabel').innerHTML = param1.replace("_"," ");
	}

	function modal_delete(param1)
	{
		document.getElementById('delete_link').href = param1;
	}

	function modal_status_active(param1)
	{
		document.getElementById('active_link').href = param1;
	}

	function modal_status_deactive(param1)
	{
		document.getElementById('deactive_link').href = param1;
	}



</script>

</html>