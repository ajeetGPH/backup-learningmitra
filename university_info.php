<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';


?>
<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
					 University Info</h3>
				</div>
			</div>
		</div>
	</div>       
	<div class="container-fluid padded">
		<div class="box">
			<?php include("message.php");?>

			<div class="box-content padded">
				<div class="tab-content">        

					<div class="tab-pane box active" id="list">
						<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
							<thead>
								<!-- <tr>
									<th><div>University Name</div></th>   
									<th><div>Rank</div></th> 
									<th><div>Country</div></th> 
									<th><div>Department</div></th> 
									<th><div>Admission</div></th> 
								</tr> -->
								<tr>
									<th>DOC Required</td>
								<?php 
								
								$user_id = $_SESSION['admin_id'];
								$query_uni_id = "SELECT universities_id FROM targeted_universities where user_id = '".$user_id."'";
								$result_uni_id = mysqli_query($con,$query_uni_id);
								$universities_id = mysqli_fetch_array($result_uni_id);
							
								$query = "SELECT * FROM universities where id IN (".$universities_id['universities_id'].")";
								$query_result = mysqli_query($con,$query); 
								
								while($universities = mysqli_fetch_array($query_result))
									{ $doc[]=$universities;
										$query =mysqli_query($con,"SELECT * FROM countries where id = '".$universities['country_id']."'");
										$country = mysqli_fetch_array($query);
										?>
										
											<th><?php echo $universities['university_name'];?> </td>
											<!-- <td><?php echo $universities['global_rank'];?> </td>
											<td><?php echo $country['name'];?> </td>
											<td><?php echo $universities['dept_link'];?> </td>
											<td><?php echo $universities['admission_link'];?> </td> -->
										
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<tr><td><div>Rank</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['global_rank'];?> </td>
									<?php } ?>
								</tr>   
								<tr><td><div>Brief History</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['history'];?> </td>
									<?php } ?>
								</tr>   
								<tr><td><div>University Website</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['website_link'];?> </td>
									<?php } ?>
								</tr>   
								 
								<tr><td><div>Department Website</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['dept_link'];?> </td>
									<?php } ?>
								</tr>   
								<tr><td><div>Course Offered</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['course'];?> </td>
									<?php } ?>
								</tr> 
								<tr><td><div>Acceptence Rate</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['acceptence_rate'];?> </td>
									<?php } ?>
								</tr>  
								<tr><td><div>Country</div></td>
									<?php foreach ($doc as $doc_req) {
										$query =mysqli_query($con,"SELECT * FROM countries where id = '".$doc_req['country_id']."'");
										$country = mysqli_fetch_array($query);
									?>
									<td><?php echo $country['name'];?> </td>
									<?php } ?>
								</tr>    
								<tr><td><div>Address</div></td>
									<?php foreach ($doc as $doc_req) {
										
									?>
									<td><?php echo $doc_req['address'];?> </td>
									<?php } ?>
								</tr>  
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php  include("copyright.php");?>
</div>

</body>
<div id="modal-search" class="modal hide fade" style="height:140px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
	</div>
	<div class="modal-delete-body" id="modal-body-delete">Are you sure, you want to Search? This is one time Search only per User.</div>
	<div class="modal-footer">
		<button onclick="getUniversity();" id="" data-dismiss="modal" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></button>
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
	</div>
</div>
