<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';


?>
<script>
	function getStates(val) {
		$.ajax({
			type: "POST",
			url: "getState.php",
			data:'country_id='+val,
			success: function(data){
				$("#state-list").html(data);
				getCity();
			}
		});
	}


	function getCity(val) {
		$.ajax({
			type: "POST",
			url: "getCity.php",
			data:'state_id='+val,
			success: function(data){
				$("#city-list").html(data);
			}
		});
	}
	function getsubject(val) {
		$.ajax({
			type: "POST",
			url: "getsubject.php",
			data:'depardment_id='+val,
			success: function(data){
				$("#subject-list").html(data);
			}
		});
	}
	


</script>
<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
						Search University </h3>
					</div>
				</div>
			</div>
		</div>       
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				
				<div class="box-content padded">
							<div class="box-content">
								<form action="search_result.php" method="post" accept-charset="utf-8" class="form-horizontal validatable" target="_top">	
									<div class="padded">                   
										<div class="control-group">
											<label class="control-label">Graduation Type</label>
											<div class="controls">                                    

												<select name="graduation_type" class="validate[required]" id="graduation_type"  onChange="select_form(this.value);">
													<option value="">
														Select
													</option>
													<option value="Undergraduate">
														Undergraduate
													</option>
													<option value="Graduate">
														Graduate
													</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Country</label>
											<div class="controls">      
												<select name="country" id="country-list" class="validate[required]" onChange="getStates(this.value);">
													<option value disabled selected>Select Country</option>
													<?php
													$query =mysqli_query($con,"SELECT * FROM countries");

													while($country = mysqli_fetch_array($query))
													{
														?>
														<option value="<?php echo $country["id"]; ?>"><?php echo $country["name"]; ?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>


										<div class="control-group">
											<label class="control-label">Department</label>
											<div class="controls">                                    

												<select name="department" id="department-list" class="validate[required]" onChange="getsubject(this.value);">
													<option value disabled selected>Select Department</option>
													<?php
													$query =mysqli_query($con,"SELECT * FROM uni_department");

													while($department = mysqli_fetch_array($query))
													{
														?>
														<option value="<?php echo $department["id"]; ?>"><?php echo $department["name"]; ?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Subject</label>
											<div class="controls">                                    


												<select name="subject" id="subject-list" class="validate[required]" onChange="">
													<option value="">Select Subject</option>

												</select>



											</div>
										</div>


									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-gray">Search</button>
										<input type="hidden" value="Search university" name="submit">
									</div>
								</form>                
							</div>                
						
						<!----CREATION FORM ENDS--->

					
				</div>
			</div>            
		</div>       
		<?php include("copyright.php");?>
	</div>
</div>
</body>

<!-----------HIDDEN MODAL FORM - COMMON IN ALL PAGES ------>
<div id="university-modal-form" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<div id="modal-tablesLabel-university" style="color:#fff; font-size:16px;">&nbsp; </div>
	</div>
	<div class="modal-body" id="modal-body-university"><?php echo constant('TI_LOADING_DATA');?></div>
	<div class="modal-footer">
		<!-- <button class="btn btn-gray" onclick="custom_print('frame1')"><?php echo constant('TI_PRINT');?></button> -->
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CLOSE');?></button>
	</div>
</div>
<!-----------HIDDEN MODAL DELETE CONFIRMATION - COMMON IN ALL PAGES ------>
<script>

	function modal_view_universiity(param1,param2 )
	{
		document.getElementById('modal-body-university').innerHTML = 
		'<iframe id="frame1" src="viewuniversity.php?id='+param2+'" width="100%" height="400" frameborder="0"></iframe>';
		document.getElementById('modal-tablesLabel-university').innerHTML = param1.replace("_"," ");
	}

	function modal(param1 ,param2 ,param3)
	{
		document.getElementById('modal-body').innerHTML = 
		'<iframe id="frame1" src="category-edit.php?c_id='+param2+'" width="100%" height="100%" frameborder="0"></iframe>';
		document.getElementById('modal-tablesLabel').innerHTML = param1.replace("_"," ");
	}
</script>

</html>