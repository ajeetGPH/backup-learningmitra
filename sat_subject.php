<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';

if (isset($_POST['submit'])) 
{ 
	
	$query_insert_subject = mysqli_query($con,"INSERT INTO subjects (subjects_status ) VALUES('1')");
	$lastinsert_id=mysqli_insert_id($con);
	for ($i = 0, $n = sizeof($languages); $i < $n; $i++)
	{
		$query_insert_subject = mysqli_query($con,"INSERT INTO subjects_description (subjects_id, languages_id, subject_name) VALUES('".$lastinsert_id."','".$languages[$i]['id']."','".$_POST['subject_name'][$languages[$i]['id']]."')");     
	}
	if($query_insert_subject)
	{
		$message_success .= constant('TI_MESSAGE_SUBJECT_SUCCESSFULLY');
	}
	else
	{
		$error .=  constant('TI_MESSAGE_SUBJECT_ERROR');
	}
}


?>

<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cube"></i>
						<?php echo constant('TI_MAIN_HEADING_SAT_SUBJECT');?> </h3>
					</div>
				</div>
			</div>
		</div>       
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				<div class="box-header">    
					<!-- <ul class="nav nav-tabs nav-tabs-left">
						<li class="active">
							<a href="#list" data-toggle="tab"><i class="icon-align-justify"></i><?php echo constant('TI_SUB_HEADING1_SUBJECT');?></a>
						</li>
						<li>
							<a href="#add" data-toggle="tab"><i class="icon-plus"></i><?php echo constant('TI_SUB_HEADING2_SUBJECT');?></a>
						</li>
					</ul>  --> 
				</div>
				<div class="box-content padded">
					<div class="tab-content">        
						<div class="tab-pane box active" id="list">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>											
										<th><div><?php echo constant('TI_TABLE_HEADING_SUBJECT_SUBJECT_NAME');?></div></th>

										<th><div><?php echo constant('TI_TABLE_HEADING_SUBJECT_MARKS');?></div></th>

										<th><div><?php echo constant('TI_TABLE_HEADING_SUBJECT_TOTAL_QUESTION');?></div></th>
										<th><div><?php echo constant('TI_TABLE_HEADING_STATUS');?></div></th>
										<th><div><?php echo constant('TI_TABLE_HEADING_OPTIONS');?></div></th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$query=mysqli_query($con,"SELECT subjects.subjects_id, subjects.subjects_status,subjects_description.subject_name,subjects_description.languages_id FROM subjects INNER join subjects_description ON subjects.subjects_id = subjects_description.subjects_id where languages_id='".$_SESSION['admin_languages_id']."'");

									while($row=mysqli_fetch_array($query))
									{
										 

										$query_question=mysqli_fetch_array(mysqli_query($con,"select SUM( total_questions ) as totalquestion from question where subjects_id='".$row['subjects_id']."'"));

												 

										$question_total_marks=mysqli_fetch_array(mysqli_query($con,"SELECT SUM(Marks) as total_mark FROM question_audio_video_paragraph where subjects_id='".$row['subjects_id']."' and languages_id='".$_SESSION['admin_languages_id']."'"));
										?>
										<tr>										
											<td><?php 
											echo $row['subject_name'];?></td>


											<td><?php if($question_total_marks['total_mark']>0){ echo $question_total_marks['total_mark'];}else{echo '0';}?></td>
											<td>
												<?php if($query_question['totalquestion']!="")
												{
													echo $query_question['totalquestion'];
												}else{ echo '0';}?>
											</td>
											<td  align="center">								
												<?php
												if($row['subjects_status']==0)
													{?>														

														<a data-toggle="modal" href="#modal-status-deactive" onclick="modal_status_deactive('subject_status.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>')" class="btn btn-red btn-small"><i class="icon-eye-close"></i> <?php echo constant('TI_BUTTON_DEACTIVATE');?></a>
													<?php }
													else
														{?>														

															<a data-toggle="modal" href="#modal-status-active" onclick="modal_status_active('subject_status.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>')" class="btn btn-green btn-small"><i class="icon-eye-open"></i> <?php echo constant('TI_BUTTON_ACTIVATE');?></a>
														<?php }?>								
													</td>
													<td align="center" class="dropdownwith">
														<a data-toggle="modal" href="subject_edit.php?subjects_id=<?php echo encrypt_string($row['subjects_id']); ?>" class="btn btn-gray btn-small"><i class="icon-pencil"></i> <?php echo constant('TI_BUTTON_EDIT');?></a>

														<a data-toggle="modal" href="#modal-delete" onclick="modal_delete('subject_delete.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>')" class="btn btn-red btn-small"><i class="icon-trash"></i> <?php echo constant('TI_BUTTON_DELETE');?></a>



														<a data-toggle="modal" href="viewquestion.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"  class="btn btn-blue btn-small"><i class="icon-zoom-in"></i> <?php echo constant('TI_BUTTON_VIEW_QUESTION');?></a>


														<select onchange="location = this.options[this.selectedIndex].value;">
															<option><?php echo constant('TI_TYPE_OF_QUESTION_ADD_QUESTION');?></option>
															<option value="question_fill_in_blanks.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_VERBLE_LAYOUT_1_2_3');?></option>
															 <!-- <option value="question_multi_fill.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_MULTI_FILL_IN_THE_BLANKS');?></option>  -->

															<!-- <option value="question_match_following.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_MATCH_THE_FOLLOWING');?></option> -->

															 <!-- <option value="question_multi_answer.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_MULTI_ANSWER');?></option> --> 
															 <option value="question_multi_answer.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_MULTI_ANSWER');?></option>
															 <option value="question_single_answer.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_SINGLE_ANSWER');?></option>
															<option value="question_analytical_one_input.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_SINGLE_ANSWER_ANALYTICAL');?></option>

														 <!-- <option value="question_paragraph.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_PARAGRAPH');?></option>  -->

															<option value="question_mixed_paragraph.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUNAT_MIXED_LAYOUT');?></option>

															

															<option value="question_one_fill_up.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_ONE_FILL_UP');?></option>

															<option value="compare_question_with_single_answer.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_COMPARE_QUESTION');?></option>
															<option value="question_verbal_gt3.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_VERBAL_GT3');?></option>

															



															<!-- <option value="question_video.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_VIDEO');?></option>

															<option value="question_audio.php?subjects_id=<?php echo encrypt_string($row['subjects_id']);?>"><?php echo constant('TI_TYPE_OF_QUESTION_AUDIO');?></option>
															 -->
															 
														</select>

</td>
												</tr>
											<?php } ?>                                
										</tbody>
									</table>
								</div>

								<div class="tab-pane box" id="add" style="padding: 5px">
									<div class="box-content">
										<form action="<?php $PHP_SELF ?>" method="post" accept-charset="utf-8" class="form-horizontal validatable" target="_top">						
											<div class="padded">                   
												<div class="control-group">
													<label class="control-label"><?php echo constant('TI_LABEL_SUBJECT_SUBJECT_NAME');?></label>
													<div class="controls">


														<?php
														for ($i = 0, $n = sizeof($languages); $i < $n; $i++) 
														{

															?>
															<input type="text" class="validate[required]" name="subject_name[<?php echo $languages[$i]['id'];?>]" placeholder="<?php echo constant('TI_PLACEHOLDER_SUBJECT_SUBJECTNAME');?>"/>&nbsp;&nbsp;<img src="include/languages/<?php echo $languages[$i]['directory'];?>/images/<?php echo $languages[$i]['image']?>" title="<?php echo $languages[$i]['name']?>"><br/><br/>

															<?php														 
														}
														?>
													</div>
												</div>
											</div>
											<div class="form-actions">
												<button type="submit" class="btn btn-gray"><?php echo constant('TI_BUTTON_SUBJECT_ADD_SUBJECT');?></button>
												<input type="hidden" value="Add new Subject" name="submit">
											</div>
										</form>                
									</div>                
								</div>            
							</div>
						</div>
					</div>            
				</div>       
				<?php include("copyright.php");?>
			</div>
		</div>
	</body>

	<!-----------HIDDEN MODAL FORM - COMMON IN ALL PAGES ------>
	<div id="modal-form" class="modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div id="modal-tablesLabel" style="color:#fff; font-size:16px;">&nbsp; </div>
		</div>
		<div class="modal-body" id="modal-body"><?php echo constant('TI_LOADING_DATA');?></div>
		<div class="modal-footer">
			<!-- <button class="btn btn-gray" onclick="custom_print('frame1')"><?php echo constant('TI_PRINT');?></button> -->
			<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CLOSE');?></button>
		</div>
	</div>
	<!-----------HIDDEN MODAL DELETE CONFIRMATION - COMMON IN ALL PAGES ------>
	<div id="modal-delete" class="modal hide fade" style="height:140px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
		</div>
		<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_POPUP_SUBJECT_DELETE');?></div>
		<div class="modal-footer">
			<a href="subject_delete.php?subjects_id=<?php echo $row['subjects_id'];?>" id="delete_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
			<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
		</div>
	</div>


	<!-----------HIDDEN MODAL ACTIVE STATUS CONFIRMATION - COMMON IN ALL PAGES ------>

	<div id="modal-status-active" class="modal hide fade" style="height:140px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
		</div>
		<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_ACTIVE');?></div>
		<div class="modal-footer">
			<a href="subject_status.php?subjects_id=<?php echo $row['subjects_id'];?>" id="active_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
			<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
		</div>
	</div>


	<div id="modal-status-deactive" class="modal hide fade" style="height:140px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
		</div>
		<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_DEACTIVE');?></div>
		<div class="modal-footer">
			<a href="subject_status.php?subjects_id=<?php echo $row['subjects_id'];?>" id="deactive_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
			<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
		</div>
	</div>

	<script>
		function modal(param1 ,param2 ,param3)
		{
			document.getElementById('modal-body').innerHTML = 
			'<iframe id="frame1" src="category-edit.php?c_id='+param2+'" width="100%" height="100%" frameborder="0"></iframe>';
			document.getElementById('modal-tablesLabel').innerHTML = param1.replace("_"," ");
		}

		function modal_delete(param1)
		{
			document.getElementById('delete_link').href = param1;
		}

		function modal_status_active(param1)
		{
			document.getElementById('active_link').href = param1;
		}

		function modal_status_deactive(param1)
		{
			document.getElementById('deactive_link').href = param1;
		}

	</script>

	</html>