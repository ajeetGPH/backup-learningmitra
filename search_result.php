<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';

if (isset($_POST['submit']) && isset($_POST['country']) || isset($_POST['graduation_type']) || isset($_POST['department']) || isset($_POST['subject'])) 
{ 

	$country_id = isset($_POST['country'])? $_POST['country']: false;	
	$graduation_type =isset($_POST['graduation_type'])? $_POST['graduation_type']: false;		
	$department_id = isset($_POST['department'])? $_POST['department']: false;	
	$subject_id = isset($_POST['subject'])? $_POST['subject']: false;	
	$status=1;

$query = "SELECT * FROM universities where graduation_type = '".$graduation_type."' or country_id = '".$country_id."' or department_id = '".$department_id."' or subject_id = '".$subject_id."' and status = '".$status."'";
//print_r($query);
$query_result = mysqli_query($con,$query); 
//print_r($query_result);
													
//print_r($universities);	

	

?>
<div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
						University Search Result </h3>
					</div>
				</div>
			</div>
		</div>       
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				
				<div class="box-content padded">
					<div class="tab-content">        

						<div class="tab-pane box active" id="list">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>
										<th><div>University Name</div></th>   
										<th><div>Rank</div></th> 
										<th><div>Country</div></th> 
										<th><div>Department</div></th> 
										<th><div>Admission</div></th> 
										<th><div>Add to favourite</div></th> 
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=0;
									while($universities = mysqli_fetch_array($query_result))
									{ $i++;
											$query =mysqli_query($con,"SELECT * FROM countries where id = '".$universities['country_id']."'");
											$country = mysqli_fetch_array($query)
										?>
										<tr>
											<td><?php echo $universities['university_name'];?> </td>
											<td><?php echo $universities['global_rank'];?> </td>
											<td><?php echo $country['name'];?> </td>
											<td><?php echo $universities['dept_link'];?> </td>
											<td><?php echo $universities['admission_link'];?> </td>
											<td align="center">
												<input type="checkbox" class='roles' name="favourite[]" id="favourite" value="<?php echo $universities['id'];?>">
											</td>
										</tr>
									<?php } ?>
									
								</tbody>
								
							</table>
							
						</div>
					</div>
				<div id="favourite_university"></div> 
				<?php } include("copyright.php");?>
			</div>
		</div>
	</div>
</div>
<script>
	$('.roles:checkbox').change(addToFav);

	function addToFav(){
				var checkedVals = $('.roles:checkbox:checked').map(function() {
				    return this.value;
				}).get();
				$.ajax({
					type: "POST",
					url: "favourite_list.php",
					data:'universities_id='+checkedVals,
					success: function(data){
						$("#favourite_university").html(data);
					}
				});

		 }       
	

</script>
</body>
