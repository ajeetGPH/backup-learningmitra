<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';

if (isset($_POST['favourite_list']) && isset($_POST['universities_id'])) 
{ 
	$universities_id = isset($_POST['universities_id'])? $_POST['universities_id']: false;
	$user_id = $_SESSION['admin_id'];
	$query_insert = "INSERT INTO targeted_universities (user_id, universities_id) VALUES('{$user_id}', '".$universities_id."') ON DUPLICATE KEY UPDATE universities_id = '".$universities_id."' ";

	if (mysqli_query($con, $query_insert)) {

		$message_success .= "University Added Successfully To Favourite List";
	}else {
			//echo "Error: " . $query . "<br>" . mysqli_error($con);
	}
	?>
	<script>
		$("#university_report").hide();
		function getUniversity() {

			if($("#gre_score").val().length > 0 && $("#toefl_score").val().length > 0 && $("#ielts_score").val().length > 0 &&  $("#gmat_score").val().length > 0 && $("#undergraduate_cgpa").val().length > 0){



			gre_score = $("#gre_score").val();
			toefl_score = $("#toefl_score").val();
			ielts_score = $("#ielts_score").val();
			gmat_score = $("#gmat_score").val();
			undergraduate_cgpa = $("#undergraduate_cgpa").val();

			$.ajax({
				type: "POST",
				url: "suggested_university.php",
				data:{gre_score: gre_score, toefl_score: toefl_score, ielts_score: ielts_score, gmat_score: gmat_score, undergraduate_cgpa: undergraduate_cgpa, search: 'search'},
				success: function(data){
					$("#suggested_university").html(data);
				}
			});

			}else{
				alert("Please provide all field details");
			}

			
		}

	</script>
	<div class="main-content">
		<div class="container-fluid" >
			<div class="row-fluid">
				<div class="area-top clearfix">
					<div class="pull-left header">
						<h3 class="title">
							<i class="icon-cubes"></i>
						Targeted Universities</h3>
					</div>
				</div>
			</div>
		</div>       
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				
				<div class="box-content padded">
					<div class="tab-content">        

						<div class="tab-pane box active" id="list">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>
										<th><div>University Name</div></th>   
										<th><div>Rank</div></th> 
										<th><div>Country</div></th> 
										<th><div>Department</div></th> 
										<th><div>Admission</div></th> 
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=0;
									$query = "SELECT * FROM universities where id IN (".$universities_id.")";
									$query_result = mysqli_query($con,$query); 
									while($universities = mysqli_fetch_array($query_result))
										{ $i++;
											$query =mysqli_query($con,"SELECT * FROM countries where id = '".$universities['country_id']."'");
											$country = mysqli_fetch_array($query)
											?>
											<tr>
												<td><?php echo $universities['university_name'];?> </td>
												<td><?php echo $universities['global_rank'];?> </td>
												<td><?php echo $country['name'];?> </td>
												<td><?php echo $universities['dept_link'];?> </td>
												<td><?php echo $universities['admission_link'];?> </td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="container-fluid" >
							<div class="row-fluid">
								<div class="area-top clearfix">
									<div class="pull-left header">
										<h3 class="title">
											<i class="icon-cubes"></i>
										Please Submit the further details it help us to serve you better:</h3>
									</div>
								</div>
							</div>
						</div> 
						<div class="box-content">
							<div class="padded">                   
								<div class="control-group">
									<label class="control-label">GRE Score</label>
									<div class="controls">                                    
										<input type="text" class="validate[required]" name="gre_score" id="gre_score" placeholder="GRE - Score"/>&nbsp;&nbsp;<br/><br/>

									</div>
								</div>
								<div class="control-group">
									<label class="control-label">TOEFL Score</label>
									<div class="controls">      
										<input type="text" class="validate[required]" name="toefl_score" id="toefl_score" placeholder="TOEFL- Score"/>&nbsp;&nbsp;<br/><br/>
									</div>
								</div>


								<div class="control-group">
									<label class="control-label">IELTS Score</label>
									<div class="controls">                                    
										<input type="text" class="validate[required]" name="ielts_score" id="ielts_score" placeholder="IELTS- Score"/>&nbsp;&nbsp;<br/><br/>

									</div>
								</div>

								<div class="control-group">
									<label class="control-label">GMAT Score</label>
									<div class="controls">                                    
										<input type="text" class="validate[required]" name="gmat_score" id="gmat_score" placeholder="GMAT- Score"/>&nbsp;&nbsp;<br/><br/>
									</div>
								</div>										
								<div class="control-group">
									<label class="control-label">Undergraduate Score in CGPA</label>
									<div class="controls">                                    

										<input type="text" class="validate[required]" name="undergraduate_cgpa" id="undergraduate_cgpa" placeholder="Undergraduate CGPA"/>&nbsp;&nbsp;<br/><br/>

									</div>
								</div>


							</div>
							<div class="form-actions">
								
								<a data-toggle="modal" href="#modal-search" class="btn btn-gray" >Search</a>
							</div>

						</div>
						<div id="suggested_university"></div> 
			
				</div>
			</div>

		</div>
		<?php } include("copyright.php");?>
	</div>

</body>
<div id="modal-search" class="modal hide fade" style="height:140px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
	</div>
	<div class="modal-delete-body" id="modal-body-delete">Are you sure, you want to Search? This is one time Search only per User.</div>
	<div class="modal-footer">
		<button onclick="getUniversity();" id="" data-dismiss="modal" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></button>
		<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
	</div>
</div>
