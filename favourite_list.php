<?php
include('include/configure.php');
include('login_check.php');

if (isset($_POST['universities_id'])) 
{
	$universities_id = isset($_POST['universities_id'])? $_POST['universities_id']: false;
	$query = "SELECT * FROM universities where id IN ($universities_id)";

	$query_result = mysqli_query($con,$query);
?>
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-cubes"></i>
						Favourite Universities list</h3>
				</div>
			</div>
		</div>
	</div>
		<?php if (mysqli_num_rows($query_result) > 0) { ?>       
		<div class="container-fluid padded">
			<div class="box">
				
				<div class="box-content padded">
					<div class="tab-content">        

						<div class="tab-pane box active" id="list">
						<form method="post" action="targeted_universities.php" id="testform" name="details">
							<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
								<thead>
									<tr>
										<th><div>University Name</div></th>   
										<th><div>Rank</div></th> 
										<th><div>Country</div></th> 
										<th><div>Department</div></th> 
										<th><div>Admission</div></th> 
<!-- 										<th><div>Add to favourite</div></th> 
 -->									</tr>
								</thead>
								<tbody>
									<?php 
									$i=0;
									while($universities = mysqli_fetch_array($query_result))
									{ $i++;
											$query =mysqli_query($con,"SELECT * FROM countries where id = '".$universities['country_id']."'");
											$country = mysqli_fetch_array($query)
										?>
										<tr>
											<td><?php echo $universities['university_name'];?> </td>
											<td><?php echo $universities['global_rank'];?> </td>
											<td><?php echo $country['name'];?> </td>
											<td><?php echo $universities['dept_link'];?> </td>
											<td><?php echo $universities['admission_link'];?> </td>
											<!-- <td align="center">
												<input type="checkbox" class='roles' name="favourite[]" id="favourite" value="<?php echo $universities['id'];?>">
											</td> -->
										</tr>
									<?php } ?>
								</tbody>
							</table>
							<div class="form-actions">
								<input type="hidden" name="universities_id" value="<?php echo($universities_id); ?>">
								<input type="submit" name="favourite_list" value="Save List">
							</div>
							</form>

						</div>
					</div>
			</div>
		</div>
	</div>
<?php  } else{ ?>
<div>
	<h4 style="color: #9e3535">Please select at least one to save list</h4>
</div>

<?php } }?>
<script type="text/javascript">
	
</script>

