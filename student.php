<?php
include('include/configure.php');
include('login_check.php');
include('include/meta_tag.php');
include('include/main-header.php');
include('include/left-menu.php');
$languages = get_languages();
$message_success='';
$error='';
?>
<script type="text/javascript">

	$(document).ready(function() {
		$('#Loading').hide();    
	});

	function check_username(){

		var username = $("#username_check").val();
		if(username.length > 2){
			$('#Loading').show();
			$.post("check_student_name_availablity.php", {
				username: $('#username_check').val(),
			}, function(response){
				$('#Info').fadeOut();
				$('#Loading').hide();
				setTimeout("finishAjax('Info', '"+escape(response)+"')", 450);
			});
			return false;
		}
	}

	function finishAjax(id, response){
		
		$('#'+id).html(unescape(response));
		$('#'+id).fadeIn(1000);
	} 

</script>
<script>
	function getXMLHTTP() { 
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		
		return xmlhttp;
	}
	
	function getState(country_id) {		
		
		var strURL="state_find.php?country_id="+country_id;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('statediv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
	function getSubcategory(category_id) {		
		
		var strURL="subcategory_find.php?category_id="+category_id;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('subcategorydiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}

	
</script>  

<?php

if (isset($_POST['submit'])) 
{ 


	$category_id = isset($_POST['category_id'])? $_POST['category_id']: false;	
	$subcategories_id = isset($_POST['subcategories_id'])? $_POST['subcategories_id']: false;	
	$first_name = isset($_POST['first_name'])? $_POST['first_name']: false;	
	$second_name = isset($_POST['second_name'])? $_POST['second_name']: false;	
	$surname_name = isset($_POST['surname_name'])? $_POST['surname_name']: false;	
	$birth_date = isset($_POST['birth_date'])? $_POST['birth_date']: false;
	$center_id = isset($_POST['center_id'])? $_POST['center_id']: false;	
	$country = isset($_POST['country'])? $_POST['country']: false;	
	$state = isset($_POST['state'])? $_POST['state']: false;	
	$email_address = isset($_POST['email_address'])? $_POST['email_address']: false;	
	$username = isset($_POST['username'])? strtolower($_POST['username']): false;	
	$password = isset($_POST['password'])? $_POST['password']: false;
	$encrypt_password = isset($_POST['password'])? encrypt($_POST['password']): false;

	$registration_date  = 'now()';
	$email_varification=0;
	$status=1;
	$account_number=accountNumber();


	if($birth_date!="")
	{
		$dobexplode=explode("/",$birth_date);
		$dob_day=$dobexplode[1];
		$dob_month=$dobexplode[0];
		$dob_year=$dobexplode[2];
		$birthdat_date_create=$dob_year.'-'.$dob_month.'-'.$dob_day;
	}

	$query_select_student=mysqli_query($con,"SELECT * FROM student where email_address='".$email_address."'");
	if(mysqli_num_rows($query_select_student)>0)
	{
		$error .=constant('TI_REGISTRATION_ERROR_ALLREADY_EXIT');
	}
	else
	{	
		$query_student = "INSERT INTO student (category_id, subcategories_id, first_name, second_name, surname_name, birth_date, mobile_no, address, center_id, country, state, email_address, username, password, encrypt_password, registration_date, status, account_number, student_language_default_name, student_language_default_code, student_language_default_id, student_language_default_directory,mail_activation) VALUES('{$category_id}', '{$subcategories_id}','{$first_name}', '{$second_name}', '{$surname_name}', '{$birthdat_date_create}','','','{$center_id}', '{$country}', '{$state}', '{$email_address}', '{$username}',  '{$password}', '{$encrypt_password}', now(), '{$status}', '{$account_number}','','','0','',1)";

		$result_student=mysqli_query($con,$query_student);	
		$lastinsert_id=mysqli_insert_id($con);

		if($result_student)
		{
			$query_update_language=mysqli_query($con,"update student set student_language_default_name ='".$query_general_setting['g_language_default_name']."', student_language_default_code ='".$query_general_setting['g_language_default_code']."', student_language_default_id ='".$query_general_setting['g_language_default_id']."', student_language_default_directory ='".$query_general_setting['g_language_default_directory']."' where student_id='".$lastinsert_id."'");



			$message_html="";
			require 'PHPMailerAutoload.php';
			$g_smtp_host_name=$query_general_setting['g_smtp_host_name'];
			$g_smtp_user_name=$query_general_setting['g_smtp_user_name'];
			$g_smtp_password=$query_general_setting['g_smtp_password'];
			$g_smtp_port=$query_general_setting['g_smtp_port'];
			$g_smtp_secure=$query_general_setting['g_smtp_secure'];
			$g_smtp_from_mail=$query_general_setting['g_smtp_from_mail'];	
			
			$to =$email_address;
			$subject="Account details for student at "." ".$query_general_setting['g_organization'];
			$mail = new PHPMailer;
			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = $g_smtp_host_name;  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = $g_smtp_user_name;                 // SMTP username
			$mail->Password = $g_smtp_password;                           // SMTP password
			$mail->SMTPSecure = $g_smtp_secure;                           // Enable TLS encryption, `ssl` also accepted
			$mail->Port = $g_smtp_port;                                    // TCP port to connect to

			$mail->setFrom($g_smtp_from_mail, $query_general_setting['g_organization']);
			$mail->addAddress($to, $found_user['admin_name']);     // Add a recipient
			//$mail->addAddress('ellen@example.com');               // Name is optional
			$mail->addReplyTo($mail1, 'Information');
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');

			//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = $subject;
			$mail->Body='<b>Hello '.$first_name.'</b>,<br><br>Thank you for registring at TiOnlineExaminationSystem. Your account is created. <br/><br/> You may login to '.$query_general_setting['g_url'].'/student/'.'  using the following details<br><br> <b>Email:</b>'. htmlspecialchars($email_address, ENT_QUOTES).'<br><b> Password:</b> ' . htmlspecialchars($password, ENT_QUOTES).'<br/><br/><br/>Kind Regards,<br/>'.$query_general_setting['g_organization'];	
			
			$mail->send();		

			if($query_update_language)
			{
				$message_success .= constant('TI_MESSAGE_STUDENT_ADD_SUCCESS_MESSAGE');
			}

		}	
		
	}

}

?><div class="main-content">
	<div class="container-fluid" >
		<div class="row-fluid">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title">
						<i class="icon-male"></i>
						<?php echo constant('TI_MAIN_HEADING_STUDENT');?> </h3>
					</div>

				</div>
			</div>
		</div>
		
		<div class="container-fluid padded">
			<div class="box">
				<?php include("message.php");?>
				<div class="box-header">
					<ul class="nav nav-tabs nav-tabs-left">
						<li class="active">
							<a href="#list" data-toggle="tab"><i class="icon-align-justify"></i> 
								<?php echo constant('TI_SUB_HEADING1_STUDENT');?>                	</a></li>
								<li>
									<a href="#add" data-toggle="tab"><i class="icon-plus"></i>
										<?php echo constant('TI_SUB_HEADING2_STUDENT');?>              	</a></li>
									</ul>
								</div>
								<div class="box-content padded">
									<div class="tab-content">
										
										<div class="tab-pane box active" id="list">
											<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
												<thead>
													<tr>
														<th><div><?php echo constant('TI_TABLE_HEADING_STUDENT_NAME') ?></div></th>
														<th><div><?php echo constant('TI_TABLE_HEADING_STUDENT_ACCOUNTNO') ?></div></th>
														<th><div><?php echo constant('TI_TABLE_HEADING_STUDENT_CENTER_NAME') ?></div></th>
														<th><div><?php echo constant('TI_TABLE_HEADING_CATEGORY_NAME') ?></div></th> 	
														<th><div><?php echo constant('TI_TABLE_HEADING_STATUS') ?></div></th> 
														<th><div><?php echo constant('TI_TABLE_HEADING_OPTIONS');?></div></th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$query=mysqli_query($con,"select * from student");
													$i=0;
													while($row=mysqli_fetch_array($query))
													{ 

														$query_category_name=mysqli_fetch_array(mysqli_query($con,"select * from catigories_description where categories_id='".$row['category_id']."' and languages_id='".$_SESSION['admin_languages_id']."'"));

														$query_subcategory_name=mysqli_fetch_array(mysqli_query($con,"select * from subcategories_description where subcategories_id='".$row['subcategories_id']."' and languages_id='".$_SESSION['admin_languages_id']."'"));

														

														$query_center_name=mysqli_fetch_array(mysqli_query($con,"select institution_name from center where center_id='".$row['center_id']."'"));
														$i++;
														
														?>
														<tr>
															<td><?php echo $row['first_name']." ".$row['second_name']." ".$row['surname_name'];?> </td>
															<td><?php echo $row['account_number'];?> </td>
															<td><?php echo $query_center_name['institution_name'];?> </td>
															<td><?php echo $query_category_name['categories_name'];?></td>		
															<td>
																<?php
																if($row['status']==0)
																	{?>													
																		<a data-toggle="modal" href="#modal-status-deactive" onclick="modal_status_deactive('student_status.php?student_id=<?php echo encrypt_string($row['student_id']);?>')" class="btn btn-red btn-small"><i class="icon-eye-close"></i> <?php echo constant('TI_BUTTON_DEACTIVATE');?></a>
																	<?php }
																	else
																		{?>														<a data-toggle="modal" href="#modal-status-active" onclick="modal_status_active('student_status.php?student_id=<?php echo encrypt_string($row['student_id']);?>')" class="btn btn-green btn-small"><i class="icon-eye-open"></i> <?php echo constant('TI_BUTTON_ACTIVATE');?></a>
																<?php }?>	
																
															</td>

															<td align="center">
																<a data-toggle="modal" href="student_edit.php?student_id=<?php echo encrypt_string($row['student_id']);?>" class="btn btn-gray btn-small"><i class="icon-pencil"></i> <?php echo constant('TI_BUTTON_EDIT');?></a>

																<a data-toggle="modal" href="#modal-delete" onclick="modal_delete('studentdelete.php?student_id=<?php echo encrypt_string($row['student_id']);?>')" class="btn btn-red btn-small"><i class="icon-trash"></i> <?php echo constant('TI_BUTTON_DELETE');?> </a>

																<a data-toggle="modal" href="#question-modal-form" onclick="modal_view_student_profile('View_Student',<?php echo $row['student_id'];?>)" class="btn btn-blue btn-small"><i class="icon-zoom-in"></i> <?php echo constant('TI_BUTTON_VIEW');?></a>

																<!-- <a data-toggle="modal" target="_new" href="result-list.php?student_id=<?php echo encrypt_string($row['student_id']);?>&category_id=<?php echo encrypt_string($row['category_id']);?>&center_id=<?php echo encrypt_string($row['center_id']);?>&pa=1" class="btn btn-gray btn-small"><i class="icon-list-ol"></i> <?php echo constant('TI_BUTTON_MAIN_RESUTLT_LIST_OF_ATTEMPEXAM'); ?></a> -->

															</td>
														</tr>
													<?php } ?>
													
												</tbody>
											</table>
										</div>
										
										<div class="tab-pane box" id="add" style="padding: 5px">
											<div class="box-content">
												<form action="<?php $PHP_SELF ?>" method="post" accept-charset="utf-8" class="form-horizontal validatable" target="_top">
													<div class="padded">       
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_EXAM_CATEGORY_NAME');?></label>
															<div class="controls">
																<select name="category_id" class="chzn-select" onChange="getSubcategory(this.value)">
																	<option><?php echo constant('TI_DROPDOWN_EXAM_CATEGORY_NAME');?></option>  
																	<?php
																	$query=mysqli_query($con,"SELECT catigories.categories_id, catigories.categories_status,catigories_description.categories_name,catigories_description.languages_id FROM catigories INNER join catigories_description ON catigories.categories_id = catigories_description.categories_id where languages_id='".$query_general_setting['g_language_default_id']."' and categories_status=1");


																	while($result_category=mysqli_fetch_array($query))
																	{
																		?>
																		<option value="<?php echo $result_category['categories_id'];?>"><?php echo $result_category['categories_name'];?></option>
																		
																	<?php }?>
																	
																</select>
															</div>
														</div>
													</div>
													

													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_EXAM_SUB_CATEGORY_NAME');?></label>
															<div class="controls" id="subcategorydiv">
																<select name="subcategory_id" class="chzn-select">	
																	<option><?php echo constant('TI_DROPDOWN_EXAM_SELECT_CATEGORY_FIRST');?></option>			
																</select>
															</div>
														</div>
													</div>
													<div class="padded">       
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_STUDENT_PROFILE_STUDENT_FIRST_NAME');?></label>
															<div class="controls">						
																<input type="text" class="validate[required]" name="first_name"/>						
															</div>
														</div>
													</div>



													<div class="padded">       
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_STUDENT_PROFILE_STUDENT_SECOND_NAME');?></label>
															<div class="controls">						
																<input type="text" class="" name="second_name"/>						
															</div>
														</div>
													</div>

													<div class="padded">       
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_STUDENT_PROFILE_STUDENT_SURNAME');?></label>
															<div class="controls">						
																<input type="text" class="validate[required]" name="surname_name"/>						
															</div>
														</div>
													</div>
													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_LABEL_STUDENT_PROFILE_STUDENT_DATEOFBIRTH');?></label>
															<div class="controls">
																
																<input type="text" name="birth_date"  class="validate[required,custom[dateFormat]] datepicker"/>
															</div>
														</div>
													</div>
													<div class="padded">       
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_NAME_OF_INSTITUTION');?></label>
															<div class="controls">
																<select name="center_id" class="validate[required]">
																	<option value=""><?php echo constant('TI_DROPDOWN_REGISTRATION_CENTER_NAME');?></option> 
																	<?php $query_center=mysqli_query($con,"select * from center where status=1");
																	while($center_result=mysqli_fetch_array($query_center))
																	{
																		$query_country=mysqli_fetch_array(mysqli_query($con,"select * from country where country_id='".$center_result['country']."'"));

																		$query_zone=mysqli_fetch_array(mysqli_query($con,"select * from zone where zone_id='".$center_result['state']."' and country_id='".$center_result['country']."'"));

																		?>
																		<option value="<?php echo $center_result['center_id'];?>"><?php echo $center_result['institution_name'].'('.$query_zone['name'].', '.$query_country['name'].')';?></option>
																		
																	<?php }?>
																</select>
															</div>
														</div>
													</div>											

													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_COUNTRY');?></label>
															<div class="controls">
																<?php 
																$query_country=mysqli_query($con,"select * from country where status=1");
																?>
																<select name="country" onChange="getState(this.value)">
																	<option value=""><?php echo constant("TI_DROPDOWN_STUDENT_PROFILE_STUDENT_COUNTRY")?></option>
																	<?php
																	while($res_country=mysqli_fetch_array($query_country))
																	{
																		?>
																		<option value="<?php echo $res_country['country_id'];?>"><?php echo $res_country['name']; ?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
														</div>
													</div>
													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_COUNTY_STATE');?></label>
															<div class="controls" id="statediv">
																<select name="state">
																	<option value=""><?php echo constant("TI_DROPDOWN_STUDENT_REGISTRATION_SELECT_COUNTRY")?></option>
																</select>
															</div>
														</div>
													</div>

													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_EMAIL_ADDRES');?></label>
															<div class="controls">
																<input type="text" class="validate[required,custom[email]]" name="email_address"/>
															</div>
														</div>
													</div>


													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_USERNAME');?></label>
															<div class="controls">
																<div id="Info"></div>									
																<input type="text" name="username" value="" class="validate[required,minSize[8]]" onblur="return check_username();" id="username_check"/><span id="Loading"><img src="img/loader.gif" alt="" /></span>
															</div>
														</div>
													</div>
													<div class="padded">                   
														<div class="control-group">
															<label class="control-label"><?php echo constant('TI_REGISTRATION_LABEL_PASSWORD');?></label>
															<div class="controls">
																<input type="password" name="password" class="validate[required]" />
															</div>
														</div>
													</div>	
													

													<div class="form-actions">
														<button type="submit" class="btn btn-gray"><?php echo constant('TI_REGISTRATION_BUTTON_SIGNUP');?></button>
														<input type="hidden" value="Add new student" name="submit">

														
													</div>
												</form>                           
											</div>                
										</div>
										
									</div>
								</div>
							</div>            </div>       
							<?php include("copyright.php");?>        
						</div>
					</div>

				</body>


				<!-----------HIDDEN QUESTION MODAL FORM - COMMON IN ALL PAGES ------>
				<div id="question-modal-form" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<div id="modal-tablesLabel_question" style="color:#fff; font-size:16px;">&nbsp; </div>
					</div>
					<div class="modal-body" id="modal-body-question"><?php echo constant('TI_LOADING_DATA');?></div>
					<div class="modal-footer">
						<!-- <button class="btn btn-gray" onclick="custom_print('frame1')"><?php echo constant('TI_PRINT');?></button> -->
						<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CLOSE');?></button>
					</div>
				</div>

				<!-----------HIDDEN MODAL DELETE CONFIRMATION - COMMON IN ALL PAGES ------>
				<div id="modal-delete" class="modal hide fade" style="height:140px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
					</div>
					<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_POPUP_STUDENT_DELETE');?></div>
					<div class="modal-footer">
						<a href="" id="delete_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
						<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
					</div>
				</div>


				<div id="modal-status-active" class="modal hide fade" style="height:140px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
					</div>
					<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_ACTIVE');?></div>
					<div class="modal-footer">
						<a href="category_status.php?c_id=<?php echo $row['c_id'];?>" id="active_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
						<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
					</div>
				</div>


				<div id="modal-status-deactive" class="modal hide fade" style="height:140px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h6 id="modal-tablesLabel"> <i class="icon-info-sign"></i>&nbsp; <?php echo constant('TI_HEADING_POPUP_CONFIRMATION');?></h6>
					</div>
					<div class="modal-delete-body" id="modal-body-delete"><?php echo constant('TI_MESSAGE_STATUS_DATA_DEACTIVE');?></div>
					<div class="modal-footer">
						<a href="category_status.php?c_id=<?php echo $row['c_id'];?>" id="deactive_link" class="btn btn-red" ><?php echo constant('TI_BUTTON_CONFIRME');?></a>
						<button class="btn btn-default" data-dismiss="modal"><?php echo constant('TI_BUTTON_CANCEL');?></button>
					</div>
				</div>
				<script>
					function modal_delete(param1)
					{
						document.getElementById('delete_link').href = param1;
					}

					function modal_view_student_profile(param1,param2 ) 
					{
						document.getElementById('modal-body-question').innerHTML = 
						'<iframe id="frame1" src="viewstudent.php?student_id='+param2+'" width="100%" height="400" frameborder="0"></iframe>';
						document.getElementById('modal-tablesLabel_question').innerHTML = param1.replace("_"," ");
					}


					function modal_status_active(param1)
					{
						document.getElementById('active_link').href = param1;
					}

					function modal_status_deactive(param1)
					{
						document.getElementById('deactive_link').href = param1;
					}
				</script>
				
				</html>